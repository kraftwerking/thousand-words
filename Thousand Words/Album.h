//
//  Album.h
//  Thousand Words
//
//  Created by RJ Militante on 2/17/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Album : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * date;

@end
