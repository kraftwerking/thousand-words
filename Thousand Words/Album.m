//
//  Album.m
//  Thousand Words
//
//  Created by RJ Militante on 2/17/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "Album.h"


@implementation Album

@dynamic name;
@dynamic date;

@end
